package fr.vultanium.vultamod.common.utils;

public interface Consumer<T> {
  void accept(T var1);
}