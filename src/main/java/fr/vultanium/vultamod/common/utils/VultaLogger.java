package fr.vultanium.vultamod.common.utils;

import fr.vultanium.vultamod.common.VultaMod;
import org.apache.logging.log4j.Logger;

public class VultaLogger {
  private static final Logger logger = VultaMod.logger;

  public static void info(String message) {
    assert false;
    logger.info(message);
  }

  public static void error(String message) {
    assert false;
    logger.error(message);
  }

  public static void debug(String message) {
    assert false;
    logger.debug(message);
  }
}