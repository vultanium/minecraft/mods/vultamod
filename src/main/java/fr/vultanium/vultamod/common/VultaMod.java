package fr.vultanium.vultamod.common;

import fr.vultanium.vultamod.client.command.CommandVultaMod;
import fr.vultanium.vultamod.client.modules.wings.WingSettings;
import fr.vultanium.vultamod.client.modules.wings.RenderWings;
import fr.vultanium.vultamod.proxy.CommonProxy;

import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import org.apache.logging.log4j.Logger;

/**
 * VultaMod, The Vultanium's Mod
 *
 * @author kasai
 */

@Mod(
        modid = Reference.MOD_ID,
        name = Reference.NAME,
        version = Reference.VERSION,
        acceptedMinecraftVersions = Reference.MC_VERSION
)
public class VultaMod {
  @SidedProxy(
          clientSide = Reference.CLIENT_PROXY_CLASS,
          serverSide = Reference.SERVER_PROXY_CLASS
  )

  private static CommonProxy proxy = null;

  private WingSettings settings;

  public static Logger logger;

  @EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    logger = event.getModLog();

    settings = new WingSettings(new Configuration(event.getSuggestedConfigurationFile()));
    settings.loadConfig();
  }

  @EventHandler
  public void init(FMLInitializationEvent event) {
    MinecraftForge.EVENT_BUS.register(new RenderWings(settings));

    ClientCommandHandler.instance.registerCommand(new CommandVultaMod(this));
  }

  @EventHandler
  public void postInit(FMLPostInitializationEvent event) {
    proxy.postInit();
  }

  public WingSettings getSettings() {
    return settings;
  }
}