package fr.vultanium.vultamod.common;

class Reference {
    static final String MOD_ID = "vultamod";
    static final String NAME = "Vultanium's Mod";
    static final String VERSION = "1.0.0";
    static final String MC_VERSION = "[1.12.2]";
    static final String CLIENT_PROXY_CLASS = "fr.vultanium.vultamod.proxy.ClientProxy";
    static final String SERVER_PROXY_CLASS = "fr.vultanium.vultamod.proxy.ServerProxy";
}