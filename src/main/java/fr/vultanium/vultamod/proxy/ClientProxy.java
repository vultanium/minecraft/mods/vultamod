package fr.vultanium.vultamod.proxy;

import fr.vultanium.vultamod.client.modules.cape.player.PlayerInfo;
import fr.vultanium.vultamod.client.modules.cape.player.downloader.DeleteElytra;
import fr.vultanium.vultamod.client.modules.cape.render.Deadmau5;
import fr.vultanium.vultamod.client.modules.cape.render.LayerCape;
import fr.vultanium.vultamod.client.modules.cape.render.LayerElytra;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.client.renderer.entity.RenderBiped;
import net.minecraft.client.renderer.entity.RenderLiving;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.entity.player.EnumPlayerModelParts;

import net.minecraftforge.common.MinecraftForge;

import java.util.Iterator;

public class ClientProxy implements CommonProxy {
  public void postInit() {
    MinecraftForge.EVENT_BUS.register(new PlayerInfo());

    Minecraft.getMinecraft().gameSettings.setModelPartEnabled(EnumPlayerModelParts.CAPE, true);

    Iterator playerIterator = Minecraft.getMinecraft().getRenderManager().getSkinMap().values().iterator();

    while (playerIterator.hasNext()) {
      RenderPlayer player = (RenderPlayer)playerIterator.next();

      player.addLayer(new Deadmau5(player));
      player.addLayer(new LayerCape(player));
      player.addLayer(new LayerElytra(player));
    }

    playerIterator = Minecraft.getMinecraft().getRenderManager().entityRenderMap.values().iterator();

    while (playerIterator.hasNext()) {
      Render render = (Render)playerIterator.next();

      if (render instanceof RenderBiped) {
        RenderLiving living = (RenderLiving)render;

        living.addLayer(new LayerElytra(living));
      }
    }

    DeleteElytra.delete();
  }
}