package fr.vultanium.vultamod.client.gui;

import fr.vultanium.vultamod.common.VultaMod;

import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

import net.minecraftforge.fml.client.config.GuiSlider;

public class GuiVultaMod extends GuiScreen {
	private VultaMod instance;

	public GuiVultaMod(VultaMod instance) {
		this.instance = instance;
	}

	@Override
	public void initGui() {
		buttonList.add(new GuiButton(0, width / 2 - 50, height / 2 - 50, 100, 20, "Wings: "));
		buttonList.add(new GuiSlider(3, width / 2 - 50, height / 2 + 25, 100, 20, "Hue: ", "%", 0, 100, 1042F, false, true));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks) {
		drawDefaultBackground();

		super.drawScreen(mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean doesGuiPauseGame() {
		return false;
	}
}