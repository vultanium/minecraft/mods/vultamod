package fr.vultanium.vultamod.client.command;

import fr.vultanium.vultamod.common.VultaMod;
import fr.vultanium.vultamod.client.gui.GuiVultaMod;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;

import net.minecraftforge.common.MinecraftForge;

public class CommandVultaMod extends CommandBase {
	private VultaMod mod;

	public CommandVultaMod(VultaMod mod) {
		this.mod = mod;
	}

	@Override
	public String getName() {
		return "VultaMod";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/vultamod";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) {
		MinecraftForge.EVENT_BUS.register(this);

		Minecraft.getMinecraft().displayGuiScreen(new GuiVultaMod(mod));

		sender.sendMessage(new TextComponentString("VultaMod"));
	}
}