package fr.vultanium.vultamod.client.modules.wings.command;

import fr.vultanium.vultamod.common.VultaMod;
import fr.vultanium.vultamod.client.modules.wings.gui.GuiEditWings;

import net.minecraft.client.Minecraft;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class CommandEditWings extends CommandBase {
	private VultaMod mod;

	public CommandEditWings(VultaMod mod) {
		this.mod = mod;
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent event) {
		MinecraftForge.EVENT_BUS.unregister(this);
		Minecraft.getMinecraft().displayGuiScreen(new GuiEditWings(mod));
	}

	@Override
	public String getName() {
		return "vultamod";
	}

	@Override
	public String getUsage(ICommandSender sender) {
		return "/vultamod";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		MinecraftForge.EVENT_BUS.register(this);
	}
}